<?php declare(strict_types=1);


namespace Alcalx\MailInterface;


use Alcalx\MailInterface\Body\BodyPart;

interface Mail
{

    public function getHeader() : Header;

    public function getDefaultPart() : BodyPart;

    public function hasAlternatePart() : bool;

    public function getAlternatePart() : BodyPart;

}
