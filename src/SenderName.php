<?php declare(strict_types=1);


namespace Alcalx\MailInterface;


interface SenderName
{


    public function asString() : string;

}
