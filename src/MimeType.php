<?php declare(strict_types=1);


namespace Alcalx\MailInterface;


interface MimeType
{

    public function asString() : string ;

}
