<?php declare(strict_types=1);


namespace Alcalx\MailInterface;


interface Encoding
{

    public function asString() : string;

}
