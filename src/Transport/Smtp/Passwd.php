<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Transport\Smtp;


interface Passwd
{

    public function asString() : string;

}
