<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Transport\Smtp;


interface SmtpConfig
{

    public function getHost(): Host;

    public function getPort() : Port;

    public function getUsername(): Username;

    public function getPasswd() : Passwd;

}
