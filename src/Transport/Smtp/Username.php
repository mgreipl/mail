<?php
/**
 * Created by PhpStorm.
 * User: mgreipl
 * Date: 13.06.20
 * Time: 20:49
 */

namespace Alcalx\MailInterface\Transport\Smtp;


interface Username
{

    public function asString() : string;

}
