<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Transport\Smtp;


interface Port
{

    public function asInt() : int;

}
