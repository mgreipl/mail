<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Transport\Smtp;

interface Host
{

    public function asString() : string;

}
