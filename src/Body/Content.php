<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Body;


interface Content
{

    public function asString() : string;

}
