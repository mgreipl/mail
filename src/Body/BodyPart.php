<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Body;


use Alcalx\MailInterface\Encoding;
use Alcalx\MailInterface\MimeType;

interface BodyPart
{

    public function getMimeType() : MimeType;

    public function getEncoding() : Encoding;

    public function getContent() : Content;

}
