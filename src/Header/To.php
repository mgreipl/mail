<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface To
{

    public function getMailAddress() : ToAddress;

    public function hasName() : bool;

    public function getName() : ToName;

}
