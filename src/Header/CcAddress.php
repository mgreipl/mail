<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface CcAddress
{

    public function asString() : string;

}
