<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface ReplyToAddress
{

    public function asString() : string;

}
