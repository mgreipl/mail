<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface CcName
{

    public function asString() : string;

}
