<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface ToAddress
{

    public function asString() : string;

}
