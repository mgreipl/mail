<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface ToCollection
{

    /**
     * @return To[]
     */
    public function asArray() : array;

}
