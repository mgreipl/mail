<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface CcCollection
{

    /**
     * @return Cc[]
     */
    public function asArray() : array;
    
}
