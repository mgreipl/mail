<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface HeaderField
{

    public function getName() : HeaderFieldName;

    public function getBody() : HeaderFieldBody;

}
