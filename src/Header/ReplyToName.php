<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface ReplyToName
{

    public function asString() : string;

}
