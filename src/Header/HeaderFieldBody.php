<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface HeaderFieldBody
{

    public function asString() : string;

}
