<?php
/**
 * Created by PhpStorm.
 * User: mgreipl
 * Date: 20.06.20
 * Time: 11:01
 */

namespace Alcalx\MailInterface\Header;


interface Subject
{

    public function asString();

}
