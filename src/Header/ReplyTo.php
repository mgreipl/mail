<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface ReplyTo
{

    public function getAddress() : ReplyToAddress;

    public function hasName() : bool;

    public function getName() : ReplyToName ;

}
