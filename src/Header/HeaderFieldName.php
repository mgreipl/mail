<?php declare(strict_types=1);


namespace Alcalx\MailInterface\Header;


interface HeaderFieldName
{

    public function asString() : string;

}
