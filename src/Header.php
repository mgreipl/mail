<?php declare(strict_types=1);


namespace Alcalx\MailInterface;



use Alcalx\MailInterface\Header\CcCollection;
use Alcalx\MailInterface\Header\HeaderField;
use Alcalx\MailInterface\Header\ReplyTo;
use Alcalx\MailInterface\Header\Subject;
use Alcalx\MailInterface\Header\ToCollection;

interface Header
{

    public function getEncoding() : Encoding;

    public function getSenderAddress() : SenderAddress;

    public function hasSenderName() : bool;

    public function getSenderName() : SenderName;

    public function getTo() : ToCollection;

    public function hasCc() : bool ;

    public function getCc() : CcCollection;

    public function getReplyTo() : ReplyTo;

    public function getSubject() : Subject;

    public function hasAdditionalHeaders() : bool;

    /**
     * @return HeaderField[]
     */
    public function getAdditionalHeaders() : array;
}
