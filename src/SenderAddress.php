<?php declare(strict_types=1);


namespace Alcalx\MailInterface;


interface SenderAddress
{

    public function asString() : string;

}
