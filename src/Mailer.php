<?php declare(strict_types=1);


namespace Alcalx\MailInterface;


interface Mailer
{

    /**
     * @throws \Throwable
     */
    public function send(Mail $mail) : void;

}
